// *** APIs ***
// clima, previsão 12 horas e previsão 5 dias: https://developer.accuweather.com/apis OK
// pegar coordenadas geográficas pelo nome da cidade: https://docs.mapbox.com/api/ OK
// pegar coordenadas do IP: http://www.geoplugin.net OK
// gerar gráficos em JS: https://www.highcharts.com/demo


$(function(){

//Pega os dados de acesso e atualiza os dados de acordo com o IP
var latitude = geoplugin_latitude();
var longitude = geoplugin_longitude();
getCityData(longitude, latitude);

//Botão para pesquisar local e atualizar dados
$("#search-button").click(function(){
    searchLongitudeLatitude($("#local").val(), getCityData);
});

$("#local").keydown(function(e){
    if(e.keyCode == 13){
        searchLongitudeLatitude($("#local").val(), getCityData);
    }
});

//Métodos pra pegar lat e long pelo ip (lançamento inicial) e variaveis de uso global;
var semana = ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"];
var listaInfo5dias = $(".day_inner .dayname");
var listaTemperatura5Dias = $(".day_inner .max_min_temp");
var lista5icones = $(".day_inner .daily_weather_icon");
var placeName;

//Função para pegar latitude e longitude baseado na string de pesquisa
function searchLongitudeLatitude(searchString, callback){
    $.ajax({
        url: "https://api.mapbox.com/geocoding/v5/mapbox.places/"+ searchString +".json?access_token=pk.eyJ1IjoibWF0dGZpY2tlIiwiYSI6ImNqNnM2YmFoNzAwcTMzM214NTB1NHdwbnoifQ.Or19S7KmYPHW8YjRz82v6g&cachebuster=1582683478602&autocomplete=true",
        type: "GET",
        dataType: "JSON",                

    }).done(function(data){

        longitude = data.features[0].geometry.coordinates[1];
        latitude = data.features[0].geometry.coordinates[0];
        
        callback(latitude, longitude); 

    }).fail(function(){
        alert("Não foi possível fazer a requisição!");
    });
}

//Função para pegar o código da cidade do accuweather
function getCityData(longitudeParams, latitudeParams){
    $.ajax({
        url: "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search.json?q="+ latitudeParams +"," + longitudeParams +"&apikey=tGpPNnjcqxgXNiUMLVPdoYQNcv9usiNG",
        type: "GET",
        typeData: "JSON",
    }).done(function(data){
        
        placeName = data.LocalizedName + ", " + data.AdministrativeArea.LocalizedName + " - " + data.Country.LocalizedName;
        $("#texto_local").html(placeName);
        
        $(".refresh-loader").show()
        getWeeklyWeather(data.Key);
        getHalfDayWeather(data.Key);  
        
        setTimeout(function(){ 
            $(".refresh-loader").hide();
        }, 2000);

    }).fail(function(){
        alert("Não foi possível localizar a cidade no Accuweather");
    }); 
}

//Função para pegar dados do dia atual e 4 dias pra frente
function getWeeklyWeather(cityKey){
    $.ajax({
        url: "http://dataservice.accuweather.com/forecasts/v1/daily/5day/"+ cityKey,
        type: "GET",
        data: {
            'apikey':'tGpPNnjcqxgXNiUMLVPdoYQNcv9usiNG',
            'language': 'pt-br',
            'metric' : 'true'
        },
        dataType: "JSON",
    }).done(function(data){        
                   
        //For para preencher os dados dos 5 dias na parte de baixo
        for(let i = 0; i < listaInfo5dias.length; i++){            

            let paramImage = data.DailyForecasts[i].Day.Icon < 9 ? "0" + data.DailyForecasts[i].Day.Icon : data.DailyForecasts[i].Day.Icon;
            let linkImagem = 'https://developer.accuweather.com/sites/default/files/'+ paramImage +'-s.png';

            let tempMin = data.DailyForecasts[i].Temperature.Minimum.Value;
            let tempMax = data.DailyForecasts[i].Temperature.Maximum.Value;
            let weekDate = new Date(data.DailyForecasts[i].Date);

            //Condicional para atualizar dados da parte de cima com dados do dia
            if(i == 0){
                $("#texto_max_min").html(tempMin.toFixed(0) + "°/" + tempMax.toFixed(0) + "°");
                $("#icone_clima").css('background-image', "url("+ linkImagem +")");                
            }        
            
            $(lista5icones[i]).css('background-image', "url("+ linkImagem +")");
            $(listaInfo5dias[i]).html(semana[weekDate.getDay()]);
            $(listaTemperatura5Dias[i]).html(tempMin.toFixed(0) + "°/" + tempMax.toFixed(0) + "°");            
        }
    
    }).fail(function(){
        alert("Não foi possível localizar os dados da cidade");
    });
}

//Função para pegar dados de 12 dias.
function getHalfDayWeather(cityKey){
    $.ajax({
        url: "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/"+ cityKey,
        type: "GET",
        data: {'apikey':'tGpPNnjcqxgXNiUMLVPdoYQNcv9usiNG', 
               'language': 'pt-br',
               'metric' : 'true'
            },
        dataType: "JSON",
    }).done(function(data){

        $("#texto_temperatura").html(data[0].Temperature.Value.toFixed(0) + "°");
        $("#texto_clima").html(data[0].IconPhrase);

        setTimeout(function(){
            var dateTimes = []; 
            var temperaturesArray = [];
            for(let i = 0; i < data.length; i++){

                let dateTime = new Date(data[i].DateTime);  
                let hour = dateTime.getHours() < 10 ? "0" + dateTime.getHours().toString() : dateTime.getHours().toString();
                dateTimes.push(hour + ":00");            
                temperaturesArray.push(parseFloat(data[i].Temperature.Value.toFixed(0)));              
            }
            
            Highcharts.chart('hourly_chart', {
                chart: {
                type: 'line'
                },
                title: {
                text: 'Temperatura hora a hora'
                },
                xAxis: {
                categories: dateTimes
                },
                yAxis: {
                title: {
                    text: 'Temperatura (°C)'
                }
                },
                plotOptions: {
                line: {
                    dataLabels: {
                    enabled: true
                    },
                    enableMouseTracking: false
                }
                },
                series: [{
                name: placeName,
                data: temperaturesArray
                }]
            });
        }, 2000);
        
    }).fail(function(){
        alert("Não foi possível localizar os da cidade");
    });
}

});

//ApiKey
//tGpPNnjcqxgXNiUMLVPdoYQNcv9usiNG